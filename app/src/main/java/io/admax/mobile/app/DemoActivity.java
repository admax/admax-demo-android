/*
 *    Copyright 2018-2019 ADMAX.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.admax.mobile.app;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.criteo.publisher.advancednative.CriteoMediaView;
import com.criteo.publisher.advancednative.CriteoNativeAd;
import com.criteo.publisher.advancednative.CriteoNativeRenderer;
import com.criteo.publisher.advancednative.RendererHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.admanager.AdManagerInterstitialAdLoadCallback;
import com.google.android.gms.ads.admanager.AppEventListener;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;
import com.google.android.gms.ads.admanager.AdManagerAdView;
import com.google.android.gms.ads.admanager.AdManagerInterstitialAd;
import com.google.android.gms.ads.nativead.NativeCustomFormatAd;
import com.smartadserver.android.library.model.SASAdElement;
import com.smartadserver.android.library.model.SASAdPlacement;
import com.smartadserver.android.library.ui.SASBannerView;
import com.smartadserver.android.library.ui.SASInterstitialManager;

import org.prebid.mobile.AdSizeListener;
import org.prebid.mobile.AdUnit;
import org.prebid.mobile.AdmaxInterstitialAdListener;
import org.prebid.mobile.AnalyticsEventType;
import org.prebid.mobile.BannerAdUnit;
import org.prebid.mobile.GamAdListener;
import org.prebid.mobile.InterstitialAdUnit;
import org.prebid.mobile.LogUtil;
import org.prebid.mobile.NativeAdUnit;
import org.prebid.mobile.NativeDataAsset;
import org.prebid.mobile.NativeEventTracker;
import org.prebid.mobile.NativeImageAsset;
import org.prebid.mobile.NativeTitleAsset;
import org.prebid.mobile.OnCompleteListener;
import org.prebid.mobile.PrebidNativeAd;
import org.prebid.mobile.PrebidNativeAdEventListener;
import org.prebid.mobile.PrebidNativeAdListener;
import org.prebid.mobile.ResultCode;
import org.prebid.mobile.Util;
import org.prebid.mobile.addendum.AdViewUtils;

import io.admax.mobile.bidder.adapter.AdmaxBidderAdapter;

import static io.admax.mobile.app.Constants.AD_BIDDER_NAME;
import static io.admax.mobile.app.Constants.AD_SERVER_NAME;
import static io.admax.mobile.app.Constants.AD_SIZE_NAME;
import static io.admax.mobile.app.Constants.AD_TYPE_NAME;
import static io.admax.mobile.app.Constants.SMART_BANNER_300X250_FORMAT_ID;
import static io.admax.mobile.app.Constants.SMART_BANNER_320X50_FORMAT_ID;
import static io.admax.mobile.app.Constants.SMART_INTERSTITIAL_FORMAT_ID;
import static io.admax.mobile.app.Constants.SMART_PAGE_ID_BANNER;
import static io.admax.mobile.app.Constants.SMART_PAGE_ID_INTERSTITIAL;
import static io.admax.mobile.app.Constants.SMART_SITE_ID;
import static io.admax.mobile.app.Constants.SMART_TARGET;

import java.util.ArrayList;


public class DemoActivity extends AppCompatActivity {
    int refreshCount;
    AdUnit adUnit;
    ResultCode resultCode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        refreshCount = 0;
        setContentView(R.layout.activity_demo);
        Intent intent = getIntent();
        if ("DFP".equals(intent.getStringExtra(AD_SERVER_NAME)) && "Banner".equals(intent.getStringExtra(Constants.AD_TYPE_NAME))) {
            createDFPBanner(intent.getStringExtra(AD_SIZE_NAME), intent.getStringExtra(AD_BIDDER_NAME));
        } else if ("DFP".equals(intent.getStringExtra(AD_SERVER_NAME)) && "In App Native".equals(intent.getStringExtra(AD_TYPE_NAME))) {
            createDFPNative(intent.getStringExtra(AD_BIDDER_NAME));
        } else if ("DFP".equals(intent.getStringExtra(AD_SERVER_NAME)) && "Interstitial".equals(intent.getStringExtra(Constants.AD_TYPE_NAME))) {
            createDFPInterstitial(intent.getStringExtra(AD_BIDDER_NAME));
        } else if ("Smart".equals(intent.getStringExtra(AD_SERVER_NAME)) && "Banner".equals(intent.getStringExtra(AD_TYPE_NAME))) {
            createSmartBanner(intent.getStringExtra(AD_SIZE_NAME), intent.getStringExtra(AD_BIDDER_NAME));
        } else if ("Smart".equals(intent.getStringExtra(AD_SERVER_NAME)) && "Interstitial".equals(intent.getStringExtra(AD_TYPE_NAME))) {
            createSmartInterstitial(intent.getStringExtra(AD_BIDDER_NAME));
        }
    }

    void createDFPBanner(String size, String bidder) {
        FrameLayout adFrame = (FrameLayout) findViewById(R.id.adFrame);
        adFrame.removeAllViews();
        final AdManagerAdView dfpAdView = new AdManagerAdView(this);
        String[] wAndH = size.split("x");
        int width = Integer.valueOf(wAndH[0]);
        int height = Integer.valueOf(wAndH[1]);
        if (width == 300 && height == 250) {
            dfpAdView.setAdUnitId(Constants.DFP_BANNER_ADUNIT_ID_300x250);
            switch (bidder) {
                case "Tappx":
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_ALL_TAPPX, adFrame, width, height);
                    break;
                default:
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_ALL_APPNEXUS, adFrame, width, height);
            }

        } else if (width == 320 && height == 50) {
            dfpAdView.setAdUnitId(Constants.DFP_BANNER_ADUNIT_ID_320x50);
            switch (bidder) {
                case "Criteo":
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_320x50_CRITEO, adFrame, width, height);
                    break;
                case "Smart":
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_320x50_SMART, adFrame, width, height);
                    break;
                case "Tappx":
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_ALL_TAPPX, adFrame, width, height);
                    break;
                default:
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_ALL_APPNEXUS, adFrame, width, height);
            }

        }
        adUnit.setAdSizeListener(new AdSizeListener() {
            @Override
            public void onAdLoaded(Util.CreativeSize size, ViewGroup adContainer) {
                LogUtil.i("ADMAX onAdLoaded with Size: " + size.getWidth() + "x" + size.getHeight());
            }
        });

        dfpAdView.setAdSizes(new AdSize(width, height));
        dfpAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Util.findPrebidCreativeSize(dfpAdView, new Util.CreativeSizeCompletionHandler() {

                    @Override
                    public void onSize(final Util.CreativeSize size) {
                        if (size != null) {
                            dfpAdView.setAdSizes(new AdSize(size.getWidth(), size.getHeight()));
                        }
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(LoadAdError error) {
                super.onAdFailedToLoad(error);
            }
        });
        dfpAdView.setAppEventListener(new AppEventListener() {
            @Override
            public void onAppEvent(String eventName, String eventInfo) {
                LogUtil.i(eventName + " " + eventInfo);
                if (AnalyticsEventType.bidWon.name().equals(eventName)) {
                    adUnit.setGoogleAdServerAd(false);
                    if (!adUnit.isAdServerSdkRendering()) {
                        adUnit.loadAd();
                    }
                }
            }
        });


        adFrame.addView(dfpAdView);
        final AdManagerAdRequest.Builder builder = new AdManagerAdRequest.Builder();
        final AdManagerAdRequest request = builder.build();
        LogUtil.i("AdRequest custom targeting :", request.getCustomTargeting().toString());

        int millis = getIntent().getIntExtra(Constants.AUTO_REFRESH_NAME, 0);
        adUnit.setAutoRefreshPeriodMillis(millis);
        adUnit.fetchDemand(request, new OnCompleteListener() {
            @Override
            public void onComplete(ResultCode resultCode) {
                DemoActivity.this.resultCode = resultCode;
                LogUtil.i("AdRequest custom targeting :", request.getCustomTargeting().toString());
                dfpAdView.loadAd(request);
                refreshCount++;
            }
        });
    }

    private void inflatePrebidNativeAd(final PrebidNativeAd ad) {
        View nativeContainer = View.inflate(this, R.layout.layout_native, null);
        ad.registerView(nativeContainer, new PrebidNativeAdEventListener() {
            @Override
            public void onAdClicked() {
                Toast.makeText(DemoActivity.this, "onAdClicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdImpression() {
                Toast.makeText(DemoActivity.this, "onAdImpression", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdExpired() {
                Toast.makeText(DemoActivity.this, "onAdExpired", Toast.LENGTH_SHORT).show();
            }
        });
        ImageView icon = nativeContainer.findViewById(R.id.imgIcon);
        Util.loadImage(icon, ad.getIconUrl());
        TextView title = nativeContainer.findViewById(R.id.tvTitle);
        title.setText(ad.getTitle());
        ImageView image = nativeContainer.findViewById(R.id.imgImage);
        Util.loadImage(image, ad.getImageUrl());
        TextView description = nativeContainer.findViewById(R.id.tvDesc);
        description.setText(ad.getDescription());
        Button cta = nativeContainer.findViewById(R.id.btnCta);
        cta.setText(ad.getCallToAction());
        ((FrameLayout) DemoActivity.this.findViewById(R.id.adFrame)).addView(nativeContainer);
    }

    private void loadInAppNative(String bidder) {
        CriteoNativeRenderer criteoNativeRenderer = new CriteoNativeRenderer() {
            @NonNull
            @Override
            public View createNativeView(@NonNull Context context, @Nullable ViewGroup viewGroup) {
                return LayoutInflater.from(context).inflate(R.layout.layout_criteo_native, viewGroup, false);
            }

            @Override
            public void renderNativeView(@NonNull RendererHelper helper, @NonNull View nativeView, @NonNull CriteoNativeAd nativeAd) {
                nativeView.<TextView>findViewById(R.id.ad_title)
                        .setText(nativeAd.getTitle());
                nativeView.<TextView>findViewById(R.id.ad_description)
                        .setText(nativeAd.getDescription());
                nativeView.<TextView>findViewById(R.id.ad_attribution)
                        .setText(String.format("Ads by %s",
                                nativeAd.getAdvertiserDomain()));
                nativeView.<Button>findViewById(R.id.ad_calltoaction)
                        .setText(nativeAd.getCallToAction());

                helper.setMediaInView(nativeAd.getProductMedia(),
                        nativeView.<CriteoMediaView>findViewById(R.id.ad_media));
            }
        };

        FrameLayout adFrame = (FrameLayout) findViewById(R.id.adFrame);
        NativeAdUnit nativeAdUnit = null;
        switch (bidder) {
            case "Criteo":
                nativeAdUnit = new NativeAdUnit(Constants.PBS_CONFIG_ID_NATIVE_CRITEO, adFrame, criteoNativeRenderer);
                nativeAdUnit.setContextType(NativeAdUnit.CONTEXT_TYPE.SOCIAL_CENTRIC);
                nativeAdUnit.setPlacementType(NativeAdUnit.PLACEMENTTYPE.CONTENT_FEED);
                nativeAdUnit.setContextSubType(NativeAdUnit.CONTEXTSUBTYPE.GENERAL_SOCIAL);
                adUnit = nativeAdUnit;
                break;
            default:
                nativeAdUnit = new NativeAdUnit(Constants.PBS_CONFIG_ID_NATIVE_APPNEXUS, adFrame, criteoNativeRenderer);
                nativeAdUnit.setContextType(NativeAdUnit.CONTEXT_TYPE.SOCIAL_CENTRIC);
                nativeAdUnit.setPlacementType(NativeAdUnit.PLACEMENTTYPE.CONTENT_FEED);
                nativeAdUnit.setContextSubType(NativeAdUnit.CONTEXTSUBTYPE.GENERAL_SOCIAL);
                adUnit = nativeAdUnit;
        }

        ArrayList<NativeEventTracker.EVENT_TRACKING_METHOD> methods = new ArrayList<>();
        methods.add(NativeEventTracker.EVENT_TRACKING_METHOD.IMAGE);
        methods.add(NativeEventTracker.EVENT_TRACKING_METHOD.JS);
        try {
            NativeEventTracker tracker = new NativeEventTracker(NativeEventTracker.EVENT_TYPE.IMPRESSION, methods);
            nativeAdUnit.addEventTracker(tracker);
        } catch (Exception e) {
            e.printStackTrace();
        }

        NativeTitleAsset title = new NativeTitleAsset();
        title.setLength(90);
        title.setRequired(true);
        nativeAdUnit.addAsset(title);
        NativeImageAsset icon = new NativeImageAsset();
        icon.setImageType(NativeImageAsset.IMAGE_TYPE.ICON);
        icon.setWMin(20);
        icon.setHMin(20);
        icon.setRequired(true);
        nativeAdUnit.addAsset(icon);
        NativeImageAsset image = new NativeImageAsset();
        image.setImageType(NativeImageAsset.IMAGE_TYPE.MAIN);
        image.setHMin(200);
        image.setWMin(200);
        image.setRequired(true);
        nativeAdUnit.addAsset(image);
        NativeDataAsset data = new NativeDataAsset();
        data.setLen(90);
        data.setDataType(NativeDataAsset.DATA_TYPE.SPONSORED);
        data.setRequired(true);
        nativeAdUnit.addAsset(data);
        NativeDataAsset body = new NativeDataAsset();
        body.setRequired(true);
        body.setDataType(NativeDataAsset.DATA_TYPE.DESC);
        nativeAdUnit.addAsset(body);
        NativeDataAsset cta = new NativeDataAsset();
        cta.setRequired(true);
        cta.setDataType(NativeDataAsset.DATA_TYPE.CTATEXT);
        nativeAdUnit.addAsset(cta);

        final AdManagerAdRequest adManagerAdRequest = new AdManagerAdRequest.Builder()
                .build();

        nativeAdUnit.fetchDemand(adManagerAdRequest, new OnCompleteListener() {
            @Override
            public void onComplete(ResultCode resultCode) {
                if (resultCode == ResultCode.SUCCESS) {
                    loadDfpNative(adManagerAdRequest);
                } else {
                    Toast.makeText(DemoActivity.this, "Native Ad Unit: " + resultCode.name(), Toast.LENGTH_SHORT).show();
                }

                refreshCount++;
                DemoActivity.this.resultCode = resultCode;
            }
        });
    }

    private void loadDfpNative(AdManagerAdRequest adManagerAdRequest) {
        AdLoader adLoader = new AdLoader.Builder(this, Constants.DFP_NATIVE_NATIVE_ADUNIT_ID_APPNEXUS)
                .forCustomFormatAd("12049435", new NativeCustomFormatAd.OnCustomFormatAdLoadedListener() {
                    @Override
                    public void onCustomFormatAdLoaded(@NonNull NativeCustomFormatAd nativeCustomFormatAd) {
                        LogUtil.d("Prebid", "custom ad loaded");
                        AdViewUtils.findNative(nativeCustomFormatAd, new PrebidNativeAdListener() {
                            @Override
                            public void onPrebidNativeLoaded(PrebidNativeAd ad) {
                                LogUtil.d("Prebid", "onPrebidNativeLoaded");
                                inflatePrebidNativeAd(ad);
                            }

                            @Override
                            public void onCriteoNativeLoaded() {
                                LogUtil.d("Prebid", "onCriteoNativeLoaded");
                                adUnit.loadAd();
                            }

                            @Override
                            public void onPrebidNativeNotFound() {
                                LogUtil.d("Prebid", "onPrebidNativeNotFound");
                            }

                            @Override
                            public void onPrebidNativeNotValid() {
                                LogUtil.d("Prebid", "onPrebidNativeNotFound");
                                // show your own content
                            }
                        });
                    }
                }, new NativeCustomFormatAd.OnCustomClickListener() {
                    @Override
                    public void onCustomClick(@NonNull NativeCustomFormatAd nativeCustomFormatAd, @NonNull String s) {

                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        Toast.makeText(DemoActivity.this, "DFP onAdFailedToLoad", Toast.LENGTH_SHORT).show();

                    }
                })
                .build();

        adLoader.loadAd(adManagerAdRequest);
    }


    void createDFPNative(String bidder) {
        loadInAppNative(bidder);
    }


    void createDFPInterstitial(String bidder) {

        switch (bidder) {
            case "Criteo":
                adUnit = new InterstitialAdUnit(Constants.PBS_CONFIG_ID_INTERSTITIAL_CRITEO);
                break;
            case "Smart":
                adUnit = new InterstitialAdUnit(Constants.PBS_CONFIG_ID_INTERSTITIAL_SMART);
                break;
            case "Tappx":
                adUnit = new InterstitialAdUnit(Constants.PBS_CONFIG_ID_ALL_TAPPX);
                break;
            default:
                adUnit = new InterstitialAdUnit(Constants.PBS_CONFIG_ID_ALL_APPNEXUS);
        }

        adUnit.setAdmaxInterstitialAdListener(new AdmaxInterstitialAdListener() {
            @Override
            public void onAdClosed() {
                LogUtil.i("Interstitial ad closed !!");
            }

            @Override
            public void onAdShown() {
                LogUtil.i("Interstitial ad shown !!");
            }

            @Override
            public void onAdFailed() {
                LogUtil.i("Interstitial ad failed !!");
            }

            @Override
            public void onAdReadyToShow() {
                LogUtil.i("Interstitial ad ready to show !!");
                adUnit.show();
            }
        });
        AdManagerAdRequest.Builder builder = new AdManagerAdRequest.Builder();
        final AdManagerAdRequest request = builder.build();
        adUnit.fetchDemand(request, new OnCompleteListener() {
            @Override
            public void onComplete(ResultCode resultCode) {
                DemoActivity.this.resultCode = resultCode;
                AdManagerInterstitialAd.load(
                        DemoActivity.this,
                        Constants.DFP_INTERSTITIAL_ADUNIT_ID,
                        request,
                        new AdManagerInterstitialAdLoadCallback() {
                            @Override
                            public void onAdLoaded(@NonNull AdManagerInterstitialAd adManagerInterstitialAd) {
                                super.onAdLoaded(adManagerInterstitialAd);
                                adManagerInterstitialAd.setAppEventListener(new AppEventListener() {
                                    @Override
                                    public void onAppEvent(@NonNull String eventName, @NonNull String eventInfo) {
                                        LogUtil.i(eventName + " " + eventInfo);
                                        if (AnalyticsEventType.bidWon.name().equals(eventName)) {
                                            adUnit.setGoogleAdServerAd(false);
                                            if (!adUnit.isAdServerSdkRendering()) {
                                                adUnit.loadAd();
                                            }
                                        }
                                    }
                                });
                                adManagerInterstitialAd.show(DemoActivity.this);
                            }

                            @Override
                            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                                super.onAdFailedToLoad(loadAdError);
                                AlertDialog.Builder builder;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    builder = new AlertDialog.Builder(DemoActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                                } else {
                                    builder = new AlertDialog.Builder(DemoActivity.this);
                                }
                                builder.setTitle("Failed to load DFP interstitial ad")
                                        .setMessage("Error code: " + loadAdError.getMessage())
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            }
                        }
                );
                refreshCount++;
            }
        });

    }

    void createSmartBanner(String size, String bidder) {
        FrameLayout adFrame = (FrameLayout) findViewById(R.id.adFrame);
        adFrame.removeAllViews();

        final SASBannerView bannerView = new SASBannerView(this);
        String[] wAndH = size.split("x");
        int width = Integer.valueOf(wAndH[0]);
        int height = Integer.valueOf(wAndH[1]);
        int heightInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, getResources().getDisplayMetrics());
        bannerView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightInDp));

        // Set the listener used when load an ad on the banner
        bannerView.setBannerListener(new SASBannerView.BannerListener() {
            @Override
            public void onBannerAdLoaded(SASBannerView sasBannerView, SASAdElement sasAdElement) {
                LogUtil.i("Banner loading completed.");
            }

            @Override
            public void onBannerAdFailedToLoad(SASBannerView sasBannerView, Exception e) {
                LogUtil.i("Banner loading failed: " + e.getMessage());                FrameLayout adFrame = (FrameLayout) findViewById(R.id.adFrame);
                ((BannerAdUnit) adUnit).setGamAdListener(new GamAdListener() {
                    @Override
                    public void onAdLoaded(AdSize size) {
                        LogUtil.i("GAM Banner loaded with width: " + size.getWidth() +
                                " and height: " + size.getHeight());
                    }

                    @Override
                    public void onAdFailedToLoad(LoadAdError error) {

                    }

                    @Override
                    public void onAdClosed() {

                    }
                });
                ((BannerAdUnit) adUnit).createDFPOnlyBanner();

            }

            @Override
            public void onBannerAdClicked(SASBannerView sasBannerView) {
                LogUtil.i("Banner clicked.");
            }

            @Override
            public void onBannerAdExpanded(SASBannerView sasBannerView) {
                LogUtil.i("Banner expanded.");
            }

            @Override
            public void onBannerAdCollapsed(SASBannerView sasBannerView) {
                LogUtil.i("Banner collapsed.");
            }

            @Override
            public void onBannerAdResized(SASBannerView sasBannerView) {
                LogUtil.i("Banner resized.");
            }

            @Override
            public void onBannerAdClosed(SASBannerView sasBannerView) {
                LogUtil.i("Banner closed.");
            }

            @Override
            public void onBannerAdVideoEvent(SASBannerView sasBannerView, int i) {
                LogUtil.i("Banner video event: " + i);
            }
        });

        final SASAdPlacement adPlacement;

        if (width == 300 && height == 250) {
            adPlacement = new SASAdPlacement(SMART_SITE_ID, SMART_PAGE_ID_BANNER, SMART_BANNER_300X250_FORMAT_ID, SMART_TARGET);
            switch (bidder) {
                default:
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_ALL_APPNEXUS, adFrame, width, height);
            }
            adUnit.setGamAdUnitId(Constants.DFP_BANNER_ADUNIT_ID_300x250);
        } else if (width == 320 && height == 50) {
            adPlacement = new SASAdPlacement(SMART_SITE_ID, SMART_PAGE_ID_BANNER, SMART_BANNER_320X50_FORMAT_ID, SMART_TARGET);
            switch (bidder) {
                case "Criteo":
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_320x50_CRITEO, adFrame, width, height);
                    break;
                case "Smart":
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_320x50_SMART, adFrame, width, height);
                    break;
                default:
                    adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_ALL_APPNEXUS, adFrame, width, height);
            }

        } else {
            adPlacement = new SASAdPlacement(SMART_SITE_ID, SMART_PAGE_ID_BANNER, SMART_BANNER_300X250_FORMAT_ID, SMART_TARGET);
            adUnit = new BannerAdUnit(Constants.PBS_CONFIG_ID_ALL_APPNEXUS, adFrame, width, height);
        }
        adUnit.setAdSizeListener(new AdSizeListener() {
            @Override
            public void onAdLoaded(Util.CreativeSize size, ViewGroup adContainer) {
                LogUtil.i("ADMAX onAdLoaded with Size: " + size.getWidth() + "x" + size.getHeight());
            }
        });
        adFrame.addView(bannerView);
        int millis = getIntent().getIntExtra(Constants.AUTO_REFRESH_NAME, 0);
        adUnit.setAutoRefreshPeriodMillis(millis);
        final AdmaxBidderAdapter bidderAdapter = new AdmaxBidderAdapter(adUnit);
        adUnit.fetchDemand(bidderAdapter, new OnCompleteListener() {
            @Override
            public void onComplete(ResultCode resultCode) {
                DemoActivity.this.resultCode = resultCode;

                if (ResultCode.SUCCESS.equals(resultCode)) {
                    bannerView.loadAd(adPlacement, bidderAdapter);
                } else {
                    bannerView.loadAd(adPlacement);
                }
                refreshCount++;
            }
        });
    }


    void createSmartInterstitial(String bidder) {
        SASAdPlacement adPlacement = new SASAdPlacement(SMART_SITE_ID, SMART_PAGE_ID_INTERSTITIAL, SMART_INTERSTITIAL_FORMAT_ID, SMART_TARGET);

        // Create the interstitial manager instance
        final SASInterstitialManager interstitialManager = new SASInterstitialManager(this, adPlacement);

        // Set the interstitial manager listener.
        // Useful for instance to perform some actions as soon as the interstitial disappears.
        interstitialManager.setInterstitialListener(new SASInterstitialManager.InterstitialListener() {
            @Override
            public void onInterstitialAdLoaded(SASInterstitialManager sasInterstitialManager, SASAdElement sasAdElement) {
                LogUtil.i("Interstitial loading completed.");
                // We display the interstitial as soon as it is loaded.
                if (adUnit.isSmartAdServerSdkRendering()) {
                    interstitialManager.show();
                }
            }

            @Override
            public void onInterstitialAdFailedToLoad(SASInterstitialManager sasInterstitialManager, Exception e) {
                LogUtil.i("Interstitial loading failed: " + e.getMessage());
                ((InterstitialAdUnit)adUnit).createDFPOnlyInterstitial(DemoActivity.this);
            }

            @Override
            public void onInterstitialAdShown(SASInterstitialManager sasInterstitialManager) {
                LogUtil.i("Interstitial shown.");
            }

            @Override
            public void onInterstitialAdFailedToShow(SASInterstitialManager sasInterstitialManager, Exception e) {
                LogUtil.i("Interstitial failed to show: " + e.getMessage());
            }

            @Override
            public void onInterstitialAdClicked(SASInterstitialManager sasInterstitialManager) {
                LogUtil.i("Interstitial clicked.");
            }

            @Override
            public void onInterstitialAdDismissed(SASInterstitialManager sasInterstitialManager) {
                LogUtil.i("Interstitial dismissed.");
            }

            @Override
            public void onInterstitialAdVideoEvent(SASInterstitialManager sasInterstitialManager, int i) {
                LogUtil.i("Interstitial video event: " + i);
            }
        });

        switch (bidder) {
            case "Criteo":
                adUnit = new InterstitialAdUnit(Constants.PBS_CONFIG_ID_INTERSTITIAL_CRITEO);
                break;
            case "Smart":
                adUnit = new InterstitialAdUnit(Constants.PBS_CONFIG_ID_INTERSTITIAL_SMART);
                break;
            default:
                adUnit = new InterstitialAdUnit(Constants.PBS_CONFIG_ID_ALL_APPNEXUS);
        }
        adUnit.setGamAdUnitId(Constants.DFP_INTERSTITIAL_ADUNIT_ID);
        int millis = getIntent().getIntExtra(Constants.AUTO_REFRESH_NAME, 0);
        adUnit.setAutoRefreshPeriodMillis(millis);
        final AdmaxBidderAdapter bidderAdapter = new AdmaxBidderAdapter(adUnit);
        adUnit.fetchDemand(bidderAdapter, new OnCompleteListener() {
            @Override
            public void onComplete(ResultCode resultCode) {
                DemoActivity.this.resultCode = resultCode;
                if (ResultCode.SUCCESS.equals(resultCode)) {
                    interstitialManager.loadAd(bidderAdapter);
                } else {
                    interstitialManager.loadAd();
                }
                refreshCount++;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adUnit != null) {
            adUnit.stopAutoRefresh();
            adUnit = null;
        }
    }

    void stopAutoRefresh() {
        if (adUnit != null) {
            adUnit.stopAutoRefresh();
        }
    }
}