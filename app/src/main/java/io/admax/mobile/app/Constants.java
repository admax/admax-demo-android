/*
 *    Copyright 2018-2019 ADMAX.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.admax.mobile.app;


public class Constants {
    private Constants() {
    }

    static final String AD_TYPE_NAME = "adType";
    static final String AD_SERVER_NAME = "adServer";
    static final String AD_BIDDER_NAME = "adBidder";
    static final String AD_SIZE_NAME = "adSize";
    static final String AUTO_REFRESH_NAME = "autoRefresh";

    //ADMAX
    // Prebid server config ids
    static final String PBS_ACCOUNT_ID_ADMAX = "4803423e-c677-4993-807f-6a1554477ced";
    static final String PBS_CONFIG_ID_ALL_TAPPX = "0dd2285c-3ad3-4357-931e-142f90db2c26";
    static final String PBS_CONFIG_ID_ALL_APPNEXUS = "dbe12cc3-b986-4b92-8ddb-221b0eb302ef";
    static final String PBS_CONFIG_ID_NATIVE_APPNEXUS = "25e17008-5081-4676-94d5-923ced4359d3";
    static final String PBS_CONFIG_ID_NATIVE_CRITEO = "3a19f25f-affe-435c-a01b-876eeb5646bb";
    static final String PBS_CONFIG_ID_320x50_SMART = "fe7d0514-530c-4fb3-9a52-c91e7c426ba6";
    static final String PBS_CONFIG_ID_INTERSTITIAL_SMART = "2cd143f6-bb9d-4ca9-9c4b-acb527657177";
    static final String PBS_CONFIG_ID_320x50_CRITEO = "fb5fac4a-1910-4d3e-8a93-7bdbf6144312";
    static final String PBS_CONFIG_ID_INTERSTITIAL_CRITEO = "5ba30daf-85c5-471c-93b5-5637f3035149";

    // DFP ad unit ids
    static final String DFP_BANNER_ADUNIT_ID_320x50_ADMAX = "/21807464892/pb_admax_320x50_top";
    static final String DFP_BANNER_ADUNIT_ID_300x250_ADMAX = "/21807464892/pb_admax_300x250_top";
    static final String DFP_INTERSTITIAL_ADUNIT_ID_ADMAX = "/21807464892/pb_admax_300x600_top";
    static final String DFP_NATIVE_NATIVE_ADUNIT_ID_APPNEXUS = "/21807464892/pb_admax_native";

    static String PBS_ACCOUNT_ID = PBS_ACCOUNT_ID_ADMAX;
    // DFP ad unit ids
    static String DFP_BANNER_ADUNIT_ID_300x250 = DFP_BANNER_ADUNIT_ID_300x250_ADMAX;
//    static String DFP_BANNER_ADUNIT_ID_300x250 = "/49926454/20minutes.appli/article_android";
    static String DFP_BANNER_ADUNIT_ID_320x50 = DFP_BANNER_ADUNIT_ID_320x50_ADMAX;
    static String DFP_INTERSTITIAL_ADUNIT_ID = DFP_INTERSTITIAL_ADUNIT_ID_ADMAX;
//    static String DFP_INTERSTITIAL_ADUNIT_ID = "/49926454/20minutes.appli/interstitiel_android";

    // Smart ids
    static final int SMART_SITE_ID =  305017;
    static final String SMART_PAGE_ID_BANNER = "1109572";
    static final String SMART_PAGE_ID_INTERSTITIAL = "1109572";
    static final int SMART_BANNER_320X50_FORMAT_ID = 80250;
    static final int SMART_BANNER_300X250_FORMAT_ID = 80235;
    static final int SMART_INTERSTITIAL_FORMAT_ID = 80600;
    static final String SMART_TARGET = "";
}
