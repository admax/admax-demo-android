/*
 *    Copyright 2018-2019 ADMAX.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.admax.mobile.app;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.ads.MobileAds;
import com.smartadserver.android.library.util.SASConfiguration;

import org.prebid.mobile.Host;
import org.prebid.mobile.LogUtil;
import org.prebid.mobile.PrebidMobile;
import org.prebid.mobile.TargetingParams;

import static android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
import static android.view.WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;
import static android.view.WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON;

public class CustomApplication extends Application {

    static private final int SITE_ID = 305017; // Your SITE_ID

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            SASConfiguration.getSharedInstance().configure(this, SITE_ID);
        } catch (SASConfiguration.ConfigurationException e) {
            LogUtil.w("Smart SDK configuration failed: " + e.getMessage());
        }

        // Enable output to Android Logcat (optional)
        SASConfiguration.getSharedInstance().setLoggingEnabled(true);

        //Initialize GAM SDK
        MobileAds.initialize(this);

        //set Prebid Mobile global Settings
        //region PrebidMobile API
        PrebidMobile.setLoggingEnabled(true);
        PrebidMobile.setAdmaxExceptionLogger(new DemoAdmaxExceptionLogger());
        PrebidMobile.setPrebidServerAccountId(Constants.PBS_ACCOUNT_ID);
        PrebidMobile.setPrebidServerHost(Host.ADMAX);
        PrebidMobile.setShareGeoLocation(true);
        PrebidMobile.setApplicationContext(getApplicationContext());
        TargetingParams.setSubjectToGDPR(true);
        TargetingParams.setGDPRConsentString("CPLLDIXPLLDKBAHABAFRBnCsAP_AAH_AAAAAILtf_X__b3_j-_59f_t0eY1P9_7_v-0zjhfdt-8N2f_X_L8X42M7vF36pq4KuR4Eu3LBIQdlHOHcTUmw6okVrzPsbk2Mr7NKJ7PEmnMbO2dYGH9_n93TuZKY7__8___z__-v_v____f_r-3_3__5_X---_e_V399zLv9____39nN___9uCCYBJhqX0AXYljgybRpVCiBGFYSHQCgAooBhaJrCBlcFOyuAj1BCwAQmoCMCIEGIKMWAQACAQBIREBIAeCARAEQCAAEAKkBCAAjYBBYAWBgEAAoBoWIEUAQgSEGRwVHKYEBEi0UE9lYAlF3saYQhllgBQKP6KjARKEECwMhIWDmOAJAS4AA");
        TargetingParams.setPurposeConsents("1");

        PrebidMobile.initAdmaxConfig(this);
        //endregion
        if (BuildConfig.DEBUG) {
            sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
            this.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
                @Override
                public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                    activity.getWindow().addFlags(FLAG_TURN_SCREEN_ON | FLAG_SHOW_WHEN_LOCKED | FLAG_KEEP_SCREEN_ON);
                }

                @Override
                public void onActivityStarted(Activity activity) {

                }

                @Override
                public void onActivityResumed(Activity activity) {

                }

                @Override
                public void onActivityPaused(Activity activity) {

                }

                @Override
                public void onActivityStopped(Activity activity) {

                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

                }

                @Override
                public void onActivityDestroyed(Activity activity) {

                }
            });
        }
    }
}
