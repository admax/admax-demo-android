package io.admax.mobile.app;

import org.prebid.mobile.AdmaxExceptionLogger;

public class DemoAdmaxExceptionLogger implements AdmaxExceptionLogger {

    @Override
    public void logException(Throwable cause) {
//        Could be implemented as follows:
//        Crashlytics.logException(cause);
    }
}
