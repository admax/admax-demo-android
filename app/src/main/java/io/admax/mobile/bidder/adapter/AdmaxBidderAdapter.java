package io.admax.mobile.bidder.adapter;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.smartadserver.android.library.thirdpartybidding.SASBidderAdapter;

import org.prebid.mobile.AdUnit;
import org.prebid.mobile.AdmaxConfigUtil;
import org.prebid.mobile.LogUtil;
import org.prebid.mobile.PrebidMobile;
import org.prebid.mobile.StringUtil;

import java.util.HashMap;

import io.admax.mobile.app.BuildConfig;

public class AdmaxBidderAdapter implements SASBidderAdapter {

    // tag for logging purposes
    private static final String TAG = AdmaxBidderAdapter.class.getSimpleName();

    // the name of the winning SSP
    private String winningSSPName;

    // the winning creative ID
    private String winningCreativeId;

    // the winning header bidding cache Id
    private String hbCacheId;

    // the winning deal ID
    private String dealId;

    // the winning creative CPM
    private double price;

    // the targeting map used to generate the creative
    private String targetingMap;

    // the keyword used to target the ad
    private String keyword;


    private AdUnit admaxAdUnit;

    public AdmaxBidderAdapter(AdUnit adUnit) {
        admaxAdUnit = adUnit;
    }

    public void update(HashMap<String, String> demand) {
        String admaxConfig = PrebidMobile.getAdmaxConfig();
        String keyValuePrefix = AdmaxConfigUtil.DEFAULT_KEYVALUE_PREFIX;
        if (!StringUtil.isEmpty(admaxConfig)) {
            keyValuePrefix = AdmaxConfigUtil.getKeyvaluePrefix(admaxConfig);
        }
        winningSSPName = demand.get("hb_bidder");
        winningCreativeId = demand.get("hb_cache_id");
        hbCacheId = demand.get("hb_cache_id");
        price = Double.parseDouble(demand.get(keyValuePrefix));
        targetingMap = stringify(demand);
    }


    @NonNull
    @Override
    public String getAdapterName() {
        return "AdmaxBidderAdapter";
    }

    @NonNull
    @Override
    public CompetitionType getCompetitionType() {
        return CompetitionType.Price;
    }

    @NonNull
    @Override
    public String getWinningSSPName() {
        return winningSSPName;
    }

    @NonNull
    @Override
    public RenderingType getRenderingType() {
        return RenderingType.PrimarySDK;
    }

    @NonNull
    @Override
    public String getWinningCreativeId() {
        return winningCreativeId;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @NonNull
    @Override
    public String getCurrency() {
        return "USD";
    }

    @Nullable
    @Override
    public String getKeyword() {
        return keyword;
    }

    @Nullable
    @Override
    public String getDealId() {
        return dealId;
    }

    @Nullable
    @Override
    public String getBidderWinningAdMarkup() {
        String adm = "<script src = \"https://cdn.admaxmedia.io/creative.js\"></script>\n" +
                "<script>\n" +
                "  var ucTagData = {};\n" +
                "  ucTagData.adServerDomain = \"\";\n" +
                "  ucTagData.pubUrl = \"" + BuildConfig.APPLICATION_ID + "\";\n" +
                "  ucTagData.targetingMap = " + targetingMap + ";\n" +
                "\n" +
                "  try {\n" +
                "    ucTag.renderAd(document, ucTagData);\n" +
                "  } catch (e) {\n" +
                "    console.log(e);\n" +
                "  }\n" +
                "</script>";
        return adm;
    }

    @Override
    public void primarySDKDisplayedBidderAd() {
        LogUtil.i(TAG, "primarySDKDisplayedBidderAd() called");
        admaxAdUnit.setSmartAdServerAd(false);
        if (!admaxAdUnit.isSmartAdServerSdkRendering()) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    admaxAdUnit.loadAd();
                }
            });
        }
    }

    @Override
    public void primarySDKClickedBidderAd() {
        LogUtil.i(TAG, "primarySDKClickedBidderAd() called");
    }

    @Override
    public void primarySDKRequestedThirdPartyRendering() {
        LogUtil.i(TAG, "primarySDKRequestedThirdPartyRendering() called");
    }

    @Override
    public void primarySDKLostBidCompetition() {
        LogUtil.i(TAG, "primarySDKLostBidCompetition() called");

    }

    private String stringify(HashMap<String, String> demand) {
        int n = demand.size() - 1;
        int i = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (String key : demand.keySet()) {
            sb.append("'");
            sb.append(key);
            sb.append("':['");
            sb.append(demand.get(key));
            sb.append("']");
            if (i < n) {
                sb.append(",");
            }
            i++;
        }
        sb.append("}");
        return sb.toString();
    }
}
